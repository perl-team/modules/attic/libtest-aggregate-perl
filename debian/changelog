libtest-aggregate-perl (0.375-1) UNRELEASED; urgency=medium

  IGNORE-VERSION: 0.375-1
  PROBLEM: still fails with Test::More >= 1.3 (#832127)
           only aborts earlier

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.375.

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Mar 2018 22:06:57 +0100

libtest-aggregate-perl (0.374-1) unstable; urgency=medium

  * Import upstream version 0.374.
  * Drop spelling.patch, merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Mon, 22 Aug 2016 19:41:47 +0200

libtest-aggregate-perl (0.373-1) unstable; urgency=medium

  [ Florian Schlichting ]
  * Import upstream version 0.373

  [ Lucas Kanashiro ]
  * Bump debhelper compatibility level to 9

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Add a Build-Conflicts-Indep against packages with Test::More >= 1.3.
  * Update Contact in debian/upstream/metadata.
  * Update years of packaging copyright.
  * Drop explicit dependency on Test::More which is already satisfied
    in oldstable.
  * Fix typo in long description. Thanks to lintian.
  * Add a patch to fix a spelling mistake in the POD. Thanks to lintian.
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Fri, 22 Jul 2016 16:31:48 +0200

libtest-aggregate-perl (0.372-2) unstable; urgency=medium

  * Team upload.
  * Remove build dependency on libdata-dump-streamer-perl,
    broken by Perl 5.22. (Closes: #801660)
  * Make the package autopkgtestable.

 -- Niko Tyni <ntyni@debian.org>  Tue, 13 Oct 2015 21:53:48 +0300

libtest-aggregate-perl (0.372-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * Add debian/upstream/metadata
  * Import upstream version 0.372
  * Update versioned build dependency on libmodule-build-perl.
  * Update Upstream-Contact.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Fri, 29 May 2015 20:38:34 +0200

libtest-aggregate-perl (0.371-1) unstable; urgency=low

  * New upstream release.
    Fixes "FTBFS with verbose testing enabled"
    (Closes: #724297)

 -- gregor herrmann <gregoa@debian.org>  Tue, 24 Sep 2013 17:27:49 +0200

libtest-aggregate-perl (0.370-1) unstable; urgency=low

  * New upstream release.
  * Update years of packaging copyright.
  * Update (build) dependencies. Move Module::Build to Build-Depends and
    switch order of alternatives. Remove unversioned perl from Depends.

 -- gregor herrmann <gregoa@debian.org>  Sun, 08 Sep 2013 17:55:02 +0200

libtest-aggregate-perl (0.368-1) unstable; urgency=low

  * Import Upstream version 0.368
  * Add build-dependencies on recommended packages

 -- Florian Schlichting <fsfs@debian.org>  Wed, 21 Aug 2013 00:08:07 +0200

libtest-aggregate-perl (0.366-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 0.366.
  * Add build-dependency on libtest-trap-perl and Module::Build 0.40.
  * Bump Standards-Version to 3.9.4 (update to copyright-format 1.0).
  * Add myself to Uploaders and copyright.

 -- Florian Schlichting <fsfs@debian.org>  Sun, 11 Aug 2013 23:06:57 +0200

libtest-aggregate-perl (0.364-1) unstable; urgency=low

  * Initial release (closes: #639534).

 -- gregor herrmann <gregoa@debian.org>  Sat, 27 Aug 2011 23:04:00 +0200
